# JSONWhite

JSONWhite is a backwards-compatible JSON extension, suitable for use when you don't have a JSON5 library available.

## JSONWhite file structure

1. JSONWhite strings must not contain literal space (`\u0020`), tab (`\u0009`) and line feed (`\u0010`). Replace them with escapes.
2. JSONWhite must not use spaces between keys, values, separators, etc.
3. JSONWhite uses significant whitespace.
4. JSONWhite uses the [Whitespace encoding](https://en.wikipedia.org/wiki/Whitespace_%28programming_language%29).
    (It's about as much of a programming language as rar or zpaq.)

## JSONWhite processing

The embedded Whitespace algorithm must accept as input the JSON file itself, and output the same
JSON data but with JS-style comments (`//` single-line comments and `/* */` multi-line comments),
optionally decoding the escapes into literal spaces, tabs and line feeds.

JSONWhite decoders should, whenever possible, validate that the resulting
JSON-with-comments is equivalent to the original JSON-without-comments.